var BurndownDiagram = (function () {
    var formatFloat = d3.format('.1f');

    return function generateDiagram(elementSelector, storypoints, days, daylyCosts) {
        var idealDaylyCosts,
            xAxis,
            xAxisLabels,
            tooltipLabels;

        idealDaylyCosts = _.range(storypoints, 0, -storypoints / (days.length - 1));
        if (idealDaylyCosts.length === days.length) {
            idealDaylyCosts[idealDaylyCosts.length - 1] = 0;
        } else {
            idealDaylyCosts.push(0);
        }

        xAxis = _.range(0, days.length + 1),
        xAxisLabels = _.map(days, function (dayIso) {return dayIso.split('-').pop()});
        tooltipLabels = _.map(days, function (dayIso) {return dayIso.split('-').reverse().join('.')});

        return c3.generate({
            size: {
                height: 500
            },
            transition: {
                duration: 0
            },
            data: {
                x: 'x',
                columns: [
                    ['x'].concat(xAxis),
                    ['ideal'].concat(idealDaylyCosts),
                    ['real'].concat(daylyCosts),
                ],
                colors: {
                    real: '#00aa00',
                    ideal: '#888',
                },
                names: {
                    real: 'Реально',
                    ideal: 'По плану',
                }
            },
            axis: {
                x: {
                    tick: {
                        format: function (x) {return xAxisLabels[x]},
                        culling: {
                            max: 0
                        }
                    },
                    padding: {
                        left: 0
                    }
                },
                y: {
                    padding: {
                        top: 1,
                        bottom: 0
                    }
                }
            },
            grid: {
                y: {
                    show: true,
                },
                x: {
                    show: true,
                }
            },
            legend: {
                show: false
            },
            point: {
                show: false
            },
            tooltip: {
                format: {
                    title: function (x) {return tooltipLabels[x]},
                    value: function (y) {return formatFloat(y).replace(/\.0+$/, '')}
                }
            }
        });
    };
}());
