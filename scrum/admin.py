from django.contrib import admin

from scrum.models import Sprint, Task, TaskStatus


class SprintAdmin(admin.ModelAdmin):
    list_display = ('title', 'storypoints', 'days', 'start', 'finish', 'closed')
admin.site.register(Sprint, SprintAdmin)

class TaskAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'notes', 'cost', 'priority', 'created', 'is_complete', 'is_deleted')
admin.site.register(Task, TaskAdmin)

class TaskStatusAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'cost', 'status', 'order', 'type', 'sprint', 'task')
admin.site.register(TaskStatus, TaskStatusAdmin)

