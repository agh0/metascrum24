var gulp         = require('gulp'),
    notify       = require('gulp-notify'),
    browserify   = require('gulp-browserify'),
    rename       = require('gulp-rename'),
    uglify       = require('gulp-uglify'),
    source       = require('vinyl-source-stream'),
    watchify     = require('watchify'),
    reactify     = require('reactify'),

    stylus       = require('gulp-stylus'),
    autoprefixer = require('gulp-autoprefixer'),
    csso         = require('gulp-csso');


gulp.task('styles', function() {
    return gulp.src('static/css/handy.styl')
        .pipe(stylus({
            url: {
                name: 'embedurl',
            }
        }))
        .pipe(autoprefixer('last 2 version', 'ie >= 8', 'opera >= 12.1'))
        // .pipe(csso())
        .pipe(gulp.dest('static/build/css'))
        .pipe(notify({message: 'Styles task complete'}));
});



function reactWatch(filename) {
    var bundler = watchify('./react/' + filename + '.jsx');
    bundler.transform(reactify)
    bundler.on('update', rebundle)

    function rebundle() {
        return bundler.bundle({debug: true})
            .pipe(source('static/build/js/' + filename + '.js'))
            .pipe(gulp.dest('.'))
            .pipe(notify({message: 'React watch task complete'}));
    }
    return rebundle()
}

gulp.task('react_board', function() {reactWatch('board')});
gulp.task('react_plan', function() {reactWatch('plan')});

gulp.task('watch', function() {
    gulp.watch('static/css/**/*.styl', ['styles']);
});



gulp.task('default', ['react']);
