var React = require('react/addons'),
    _ = require('underscore'),
    $ = require('jquery'),
    uuid = require('node-uuid');

var draggedTaskUUID = null;
var TASK_STATUSES = [
    ['plan',    'В плане'],
    ['process', 'В процессе'],
    ['review',  'Ревью'],
    ['done',    'Готово']
];


var Board = React.createClass({
    render: function () {
        return (
            <div>
                <div className="state">{JSON.stringify(this.state)}</div>
                <div className="board">
                    {TASK_STATUSES.map(function(status, i) {
                        var name = status[0],
                            title = status[1],
                            tasks = this.getColumnTasks(name)
                        return (
                            <Column key={i} tasks={tasks} status={name} title={title}
                                updateTask={this.updateTask}
                                swapTasksOrder={this.swapTasksOrder} />
                        )
                    }.bind(this))}
                </div>
                <NewTask onCreate={this.createTask} />
            </div>
        );
    },
    createTask: function (task) {
        // http://blog.tompawlak.org/generate-unique-identifier-nodejs-javascript
        // http://www.broofa.com/2008/09/javascript-uuid-function/
        this.state.tasks.push(_.extend(task, {uuid: uuid.v4()}));
        this.setState({tasks: this.state.tasks});
    },
    getInitialState: function() {
        return {tasks: []};
    },
    componentWillMount: function () {
        this.setState({tasks: tasks});
    },
    componentDidUpdate: _.debounce(function () {
        var url = document.location + 'update/';
        $.post(url, {data: JSON.stringify(this.state.tasks)});
    }, 1000),
    getColumnTasks: function (status) {
        return _.filter(this.state.tasks, function (task) {
            return task.status === status;
        });
    },
    updateTask: function (taskUUID, field, value) {
        var task = _.findWhere(this.state.tasks, {'uuid': taskUUID});
        task[field] = value
        this.setState({tasks: tasks});
    },
    swapTasksOrder: function (firstUUID, secondUUID) {
        // NOTE: страшно - срефакторить
        var newState = _.clone(this.state),
            movedElement = _.findWhere(newState.tasks, {'uuid': firstUUID}),
            movedElementIndex = getElIndex(newState.tasks, firstUUID),
            afterElementIndex = getElIndex(newState.tasks, secondUUID);
        newState.tasks.splice(movedElementIndex, 1);
        newState.tasks.splice(afterElementIndex, 0, movedElement);
        this.setState(newState);
    }
});


var Column = React.createClass({
    dragOver: function (e) {
        e.preventDefault();
    },
    drop: function (e) {
        this.updateTaskStatus();
        draggedTaskUUID = null;
    },
    updateTaskStatus: function () {
        this.props.updateTask(draggedTaskUUID, 'status', this.props.status);
    },
    render: function () {
        return (
            <div className="board__column"
                onDragOver={this.dragOver}
                onDragEnter={this.updateTaskStatus}
                onDrop={this.drop}>
                <div className="board__column__title">{this.props.title}</div>
                <div className="board__column__tasks">
                    {this.props.tasks.map(function (task, i) {
                        return React.createElement(Task, (_.extend(task, {
                            key:            i,
                            updateTask:     this.props.updateTask,
                            swapTasksOrder: this.props.swapTasksOrder
                        })));
                    }.bind(this))}
                </div>
            </div>
        );
    }
});


var Task = React.createClass({
    render: function () {
        var classes = draggedTaskUUID === this.props.uuid ? ' task-ghost' : '';
        return (
            <div className={'task task-' + this.props.type + classes}
                draggable="true"
                onDragStart={this.dragStart}
                onDragOver={this.dragEnter}>
                <div className="task__inner">
                    {this.props.type !== 'moar' ?
                        <input className="task__cost" type="text"
                            value={this.props.cost}
                            onChange={this.changeCost} />
                    : ''}
                    <span className="task__title">{this.props.title}</span>
                    <p>{this.props.description}</p>
                </div>
            </div>
        );
    },
    dragStart: function (e) {
        e.dataTransfer.effectAllowed = 'move';
        draggedTaskUUID = this.props.uuid;
    },
    dragEnter: function (e) {
        if (draggedTaskUUID !== this.props.uuid) {
            this.props.swapTasksOrder(draggedTaskUUID, this.props.uuid);
        }
    },
    changeCost: function (e) {
        this.props.updateTask(this.props.uuid, 'cost', e.target.value);
    }
});


var NewTask = React.createClass({
    getInitialState: function () {
        return {visible: false};
    },
    showAdder: function () {
        this.setState({visible: true});
    },
    hidePopup: function () {
        this.setState({visible: false});
    },
    render: function () {
        return (
            <div>
                <br />
                <button type="button" className="button" onClick={this.showAdder}>Добавить красненькую задачу</button>
                {this.state.visible ? <TaskPopup onClose={this.hidePopup} onCreate={this.props.onCreate} /> : ''}
            </div>
        )
    }
});

var TaskPopup = React.createClass({
    mixins: [React.addons.LinkedStateMixin],
    getInitialState: function () {
        return {title: '', description: '', notes: '', status: 'plan', type: 'moar'};
    },
    submit: function (e) {
        e.preventDefault();

        this.props.onCreate(this.state);
        this.props.onClose();
    },
    render: function () {
        return (
            <div className="popup">
                <div className="popup__content">
                    <form onSubmit={this.submit}>
                        <div className="form form-popup">
                            <div className="form__control">
                                <label className="form__label">Название</label>
                                <div className="form__wrap">
                                    <input type="text" className="form__field" valueLink={this.linkState('title')} />
                                </div>
                            </div>
                            <div className="form__control">
                                <label className="form__label">Описание</label>
                                <div className="form__wrap">
                                    <textarea rows="4" className="form__field form__field-textarea" valueLink={this.linkState('description')} />
                                </div>
                            </div>
                            <div className="form__footer">
                                <button className="button button-orange button-big" type="submit">Создать</button>
                            </div>
                        </div>
                    </form>
                    <span className="popup__close" onClick={this.props.onClose}></span>
                </div>
            </div>
        )
    }
});


React.render(
    <Board tasks={tasks} />,
    document.getElementById('board_place')
);


function getElIndex(list, uuid) {
    for (var i = 0; i < list.length; i++) {
        if (list[i].uuid === uuid) {
            return i;
        }
    }
    return -1;
}
