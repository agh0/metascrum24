# -*- coding: utf-8 -*-
from datetime import datetime

from django.db import models
from handy.models import StringArrayField

class TaskStatuses:
    PLAN, PROCESS, REVIEW, DONE = range(0, 4)
    choices = (
        (PLAN,    'plan'),
        (PROCESS, 'process'),
        (REVIEW,  'review'),
        (DONE,    'done'),
    )

class TaskTypes:
    USUAL, MOAR, HOT = range(0, 3)
    choices = (
        (USUAL, 'usual'),
        (MOAR,  'moar'),
        (HOT,   'hot'),
    )


class Sprint(models.Model):
    title = models.CharField(max_length=100, blank=True, default=None)
    storypoints = models.PositiveSmallIntegerField(max_length=2, blank=True, default=0)
    days = StringArrayField(max_length=30)
    start = models.DateField()
    finish = models.DateField(null=True, blank=True)
    closed = models.DateField(null=True, blank=True)

    def __unicode__(self):
        return self.title

    def days_as_dates(self):
        return [datetime.strptime(day, '%Y-%m-%d').date() for day in self.days]

    def cost_statistics(self):
        dayly_cost = dict(self.taskstatus_set.filter(type=TaskTypes.USUAL) \
                              .exclude(status=TaskStatuses.DONE) \
                              .values_list('timestamp').annotate(cost=models.Sum('cost')))
        return [dayly_cost[day] for day in self.days_as_dates() if day in dayly_cost]

    class Meta:
        ordering = ('-id',)
        get_latest_by = 'id'
        verbose_name = u'sprint'
        verbose_name_plural = u'sprints'


class Task(models.Model):
    title = models.CharField(max_length=300, blank=True, default=None)
    description = models.TextField(blank=True, default=None)
    notes = models.TextField(blank=True, default=None)
    cost = models.PositiveSmallIntegerField(max_length=2, blank=True, default=0)
    priority = models.PositiveSmallIntegerField(max_length=2, blank=True, default=0)
    created = models.DateField(auto_now_add=True)
    is_complete = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ('-id',)
        verbose_name = u'task'
        verbose_name_plural = u'tasks'


class TaskStatus(models.Model):
    timestamp = models.DateField()
    cost = models.PositiveSmallIntegerField(max_length=2, blank=True, default=0)
    status = models.PositiveSmallIntegerField(max_length=1, blank=True, default=0, choices=TaskStatuses.choices)
    order = models.PositiveSmallIntegerField(blank=True, default=0)
    type = models.PositiveSmallIntegerField(max_length=1, blank=True, default=0, choices=TaskTypes.choices)
    sprint = models.ForeignKey(Sprint)
    task = models.ForeignKey(Task)

    class Meta:
        verbose_name = u'task status'
        verbose_name_plural = u'task statuses'
        unique_together = [
            ('sprint', 'task', 'timestamp'),
        ]

"""
Генерация начальных данных

from __future__ import unicode_literals
from datetime import datetime, timedelta
from django.utils import timezone
from scrum.models import Sprint, Task, TaskStatus

tasks_data = [{
    'title': 'Переход к джанго 1.4',
    'description': 'Нужно перейти к джанго 1.4',
    'notes': '',
    'cost': 3,
    'priority': 1,
}, {
    'title': 'Раздел "ремонт"',
    'description': 'Захуярить как можно быстрее',
    'notes': 'Сделать чтобы работало',
    'cost': 5,
    'priority': 2,
}, {
    'title': 'Кнопка "заказаьть фотосессию"',
    'description': 'ТЗ брать у Даши',
    'notes': '',
    'cost': 1,
    'priority': 3,
}]

tasks = []
for item_data in tasks_data:
    task = Task(created=timezone.now(), is_complete=False, is_deleted=False, **item_data)
    task.save()
    tasks.append(task)

days = ['2014-11-%02d' % day for day in xrange(1, 11)]
date_format = '%Y-%m-%d'
sprint = Sprint(title="Спринт №1", storypoints=9, days=days, start=datetime.strptime(days[0], date_format).date(),
                finish=datetime.strptime(days[-1], date_format).date())
sprint.save()

task_statuses_data = [{
    'cost': 2,
    'status': 1,
    'order': 1,
    'task': tasks[0],
}, {
    'cost': 5,
    'status': 0,
    'order': 3,
    'task': tasks[1],
}, {
    'cost': 1,
    'status': 0,
    'order': 2,
    'task': tasks[2],
}]

yesterday_task_statuses_data = [{
    'cost': 3,
    'status': 0,
    'order': 1,
    'task': tasks[0],
}, {
    'cost': 5,
    'status': 0,
    'order': 3,
    'task': tasks[1],
}, {
    'cost': 1,
    'status': 0,
    'order': 2,
    'task': tasks[2],
}]

now = timezone.now()
for task_status_data in yesterday_task_statuses_data:
    task_status = TaskStatus(timestamp=now-timedelta(days=1), sprint=sprint, type=0, **task_status_data)
    task_status.save()

for task_status_data in task_statuses_data:
    task_status = TaskStatus(timestamp=now, sprint=sprint, type=0, **task_status_data)
    task_status.save()
"""
