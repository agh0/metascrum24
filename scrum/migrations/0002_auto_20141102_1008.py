# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('scrum', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='taskstatus',
            name='timestamp',
            field=models.DateField(),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='taskstatus',
            unique_together=set([('sprint', 'task', 'timestamp')]),
        ),
    ]
