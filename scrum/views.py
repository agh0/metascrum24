# coding: utf-8
from datetime import datetime, timedelta, date
import json

from django.db.models import Max
from django.shortcuts import get_object_or_404
from django.utils.safestring import mark_safe

from handy.decorators import render_to, render_to_json
from handy.ajax import ajax
from scrum.models import Sprint, TaskStatus, Task


@render_to('scrum/sprint.j2')
def sprint(request, sprint_id):
    sprint = get_object_or_404(Sprint.objects, id=sprint_id)

    latest_statuses_ids = Task.objects.filter(taskstatus__sprint=sprint) \
                                      .annotate(latest_status=Max('taskstatus__id')) \
                                      .values_list('latest_status', flat=True)
    latest_statuses = TaskStatus.objects.select_related('task').filter(id__in=latest_statuses_ids)
    prepared_statuses = [{
        'uuid': status.id,
        'title': status.task.title,
        'description': status.task.description,
        'notes': status.task.notes,
        'status': status.get_status_display(),
        'cost': status.cost,
        'order': status.order,
        'type': status.get_type_display(),
    } for status in latest_statuses]

    return {
        'sprint': sprint,
        'statuses': json.dumps(prepared_statuses)
    }

@ajax
def add_task(request, task_id):
    pass

@ajax
def edit_task(request, task_id):
    pass

@ajax
def delete_task(request, task_id):
    task = get_object_or_404(Task, id=task_id)
    task.delete()

@render_to('scrum/sprint_add.j2')
def sprint_add(request):
    def active_sprint():
        try:
            today = date.today()
            return Sprint.objects.get(closed__isnull=True)
        except Sprint.DoesNotExist:
            return None

    def create_sprint():
        today = date.today()
        next_monday = today + timedelta(days=-today.weekday(), weeks=1)
        days = [day for day in [next_monday + timedelta(days=i) for i in range(14)] \
                if day.weekday() not in {5, 6}]
        sprint = Sprint(title=u'Спринт №', storypoints=22, days=days, start=next_monday)
        sprint.save()
        return sprint

    sprint = active_sprint() or create_sprint()

    tasks = [{
        'id': task.id,
        'title': task.title,
        'description': task.description,
        'notes': task.notes,
        'cost': task.cost,
        'priority': task.priority,
        'created': str(task.created),
        'in_sprint': False
    } for task in Task.objects.filter(is_complete=False, is_deleted=False).all()]

    prepared_tasks = {
        'sprint': sprint.id,
        'storypoints': sprint.storypoints,
        'tasks': tasks,
    }

    return {
        'sprint': sprint,
        'prepared_tasks': json.dumps(prepared_tasks),
    }


@render_to_json()
def sprint_update(request, sprint_id):
    data = request.POST.get('data')
    return {'success': True}


@render_to('scrum/burndown.j2')
def sprint_burndown(request, sprint_id):
    sprint = get_object_or_404(Sprint.objects, id=sprint_id)
    return {
        'sprint_title': sprint.title,
        'sprint_storypoints': json.dumps(sprint.storypoints),
        'sprint_days': json.dumps(sprint.days),
        'task_dayly_costs': json.dumps(sprint.cost_statistics()),
    }

